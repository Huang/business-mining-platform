<?php

$fp = fopen($argv[1], 'r');
$buf = fread($fp, filesize($argv[1]));
fclose($fp);
$pos = 0;
$flag = false;
set_error_handler('handleError');

$fp = fopen(trim($argv[1]).'.json', 'w');
fwrite($fp, '[');
preg_match_all('/([0-9]+\ [0-9a-zA-Z][0-9]{7})/', $buf, $matches, PREG_OFFSET_CAPTURE);
$matches[0][count($matches[0])] = array('', filesize($argv[1]));

for ($i = 1 ; $i < count($matches[0]) ; $i++) {
  $txt = trim(str_replace("\n", " ", substr($buf, $matches[0][$i-1][1], $matches[0][$i][1] - $matches[0][$i-1][1])));
  $txt = preg_replace('/(營利事業|商業登記).*登記清冊.*文\ 號/', '', $txt);
  $txt = preg_replace('/([0-9]+\ [0-9]{8}(?! ))/', '${1} ', $txt);

  if (!trim($txt))
    continue;

  $token = explode(" ", trim($txt));
  $token_start = 3;
  $token_end = count($token);

  try {
    $dict = array(
      "sn" => $token[1], 
      "name" => $token[2], 
      "addr" => get_addr_token($token, $token_start, $token_end),
      "capital" => get_capital_token($token, $token_start, $token_end),
      "org_type" => get_org_type_token($token, $token_start, $token_end),
      "representative" => get_representative_token($token, $token_start, $token_end),
      "start" => get_start_date_token($token, $token_start, $token_end),
      "reg_number" => get_reg_number_token($token, $token_start, $token_end),
    );
  } catch (Exception $e) {
    #var_dump($token);
    #sleep(1);
  }
  if ($dict['start'])
    fwrite($fp, json_encode($dict) . ',');
}

fseek($fp, -1, SEEK_CUR);
fwrite($fp, ']');
fclose($fp);

function get_addr_token($arr, $start, $end) {
  $buf = "";
  for ($a = $start ; $a < $end ; $a++)
    $buf .= trim($arr[$a]);
  $c = strpos($buf, '號');
  $buf = substr($buf, 0, 3+$c);
  return $buf;
}

function get_capital_token($arr, $start, $end) {
  for ($a = $start ; $a < $end ; $a++) {
    $b = str_replace(',', '', $arr[$a]);
    if (is_numeric($b))
      return $b;
  }
  return -1;
}

function get_reg_number_token($arr, $start, $end) {
  for ($a = $start ; $a < $end ; $a++) {
    preg_match('/[0-9]{4,}/', $arr[$a], $matches, PREG_OFFSET_CAPTURE);
    if (strlen($arr[$a]) > 8 && count($matches) > 0)
      return $arr[$a];
  }
  return "";
}

function get_start_date_token($arr, $start, $end) {
  for ($a = $start ; $a < $end ; $a++) {
    preg_match('/[0-9]{3}\/[0-9]{2}\/[0-9]{2}/', $arr[$a], $matches, PREG_OFFSET_CAPTURE);
    if (count($matches) > 0)
      return $matches[0][0];
  }
  return "";
}

function get_org_type_token($arr, $start, $end) {
  for ($a = $start ; $a < $end ; $a++) {
    $b = str_replace(',', '', $arr[$a]);
    if (is_numeric($b))
      return $arr[$a+1];
  }
  return "";
}

function get_representative_token($arr, $start, $end) {
  for ($a = $start ; $a < $end ; $a++) {
    $b = str_replace(',', '', $arr[$a]);
    if (is_numeric($b))
      break;
  }

  $buf = "";
  for ( $a += 2 ; $a < $end ; $a++)
    $buf .= trim($arr[$a]);

  preg_match('/[0-9]{3}\/[0-9]{2}\/[0-9]{2}/', $buf, $matches, PREG_OFFSET_CAPTURE);
  if (count($matches) > 0)
    $buf = substr($buf, 0, $matches[0][1]);
  return $buf;
}

function handleError() {
  throw new ErrorException();
}
