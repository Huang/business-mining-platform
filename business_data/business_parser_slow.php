<?php

$fp = fopen($argv[1], 'r');
$buf = fread($fp, filesize($argv[1]));
fclose($fp);
$pos = 0;
$flag = false;
set_error_handler('handleError');

$fp = fopen(trim($argv[1]).'.json', 'w');
fwrite($fp, '[');
while (1) {
  preg_match('/([0-9]+)\ [0-9]{8}/', $buf, $matches, PREG_OFFSET_CAPTURE, 8);

  if (count($matches) < 1)
    break;

  #echo "pos = {$matches[0][1]}\n";
  #echo "mat = {$matches[0][0]}\n";
  $pos = $matches[0][1];

  $txt = trim(str_replace("\n", " ", substr($buf, 0, $pos)));
  $buf = substr($buf, $pos);
  $txt = preg_replace('/(營利事業|商業登記).*登記清冊.*文\ 號/', '', $txt);
  $txt = preg_replace('/([0-9]+\ [0-9]{8}(?! ))/', '${1} ', $txt);

  if (!trim($txt))
    continue;

  $token = explode(" ", trim($txt));

  try {
    $dict = array(
      "sn" => $token[1], 
      "name" => $token[2], 
      "addr" => get_addr_token($token, 3, count($token)-5), 
      "capital" => get_capital_token($token, 3, count($token)), 
      "org_type" => get_org_type_token($token, 3, count($token)),
      "representative" => get_representative_token($token, 3, count($token)-2), 
      "start" => $token[count($token)-2], 
      "reg_number" => $token[count($token)-1]
    );
  } catch (Exception $e) {
    var_dump($token);
    sleep(1);
  }
  fwrite($fp, json_encode($dict) . ',');
  #echo "$txt\n";
}

fwrite($fp, '{}]');
fclose($fp);

function get_addr_token($arr, $start, $end) {
  $buf = "";
  for ($a = $start ; $a < $end ; $a++)
    $buf .= trim($arr[$a]);
  $c = strpos($buf, '號');
  $buf = substr($buf, 0, 3+$c);
  return $buf;
}

function get_capital_token($arr, $start, $end) {
  for ($a = $start ; $a < $end ; $a++)
    if (is_numeric($arr[$a]))
      return (int)$arr[$a];
  return -1;
}

function get_org_type_token($arr, $start, $end) {
  for ($a = $start ; $a < $end ; $a++)
    if (is_numeric($arr[$a]))
      return $arr[$a+1];
  return "";
}

function get_representative_token($arr, $start, $end) {
  for ($a = $start ; $a < $end ; $a++)
    if (is_numeric($arr[$a]))
      break;

  $buf = "";
  for ( $a += 2 ; $a < $end ; $a++)
    $buf .= trim($arr[$a]);
  return $buf;
}

function handleError() {
  throw new ErrorException();
}
