<?php
$input_json = $argv[1];
$fs = fopen($input_json, 'r');
$json = json_decode(fread($fs, filesize($input_json)), true);
fclose($fs);
$match = $not_match = 0;
foreach ($json as $k=>$j) {
  $sha1 = sha1($j['addr']);
  $geo_json = "addr_trans/{$sha1}";
  if (is_file($geo_json) && filesize($geo_json)) {
    $match += 1;
    $fs = fopen($geo_json, 'r');
    $geo_arr = json_decode(fread($fs, filesize($geo_json)), true);
    fclose($fs);
    if ($geo_arr['status'] == 'OK') {
      #echo "\033[1;32m";
      #var_dump($geo_arr['results'][0]['formatted_address']);
      #var_dump($geo_arr['results'][0]['geometry']['location']);
      #echo "\033[0m";

      $json[$k]['addr'] = $geo_arr['results'][0]['formatted_address'];
      $json[$k]['latlng'] = $geo_arr['results'][0]['geometry']['location'];
    }
  } else {
    $not_match += 1;
    echo "\033[1;31m";
    var_dump($j['addr']);
    echo "\033[0m";
  }
}
var_dump($match);
var_dump($not_match);
$fs = fopen("{$input_json}.with_latlng.json", 'w');
fwrite($fs, json_encode($json));
fclose($fs);
