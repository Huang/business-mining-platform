<?php
$sock5 = "";
if ($argc > 1)
  $sock5 = '--socks5 localhost:' . (int)$argv[1];

$addrs = file('json/all_addr');
shuffle($addrs);

foreach($addrs as $v) {
  $addr = trim($v);
  $sha1_fn = sha1($addr);

  if ( ! is_file("addr_trans/{$sha1_fn}") ) {
    $urlenc_addr = urlencode($addr);
    echo "{$addr}\n";
    system("curl -s {$sock5} -o addr_trans/{$sha1_fn} 'http://maps.googleapis.com/maps/api/geocode/json?address={$urlenc_addr}&sensor=false&language=zh-TW'", $ret);
    if ($ret == 7)
      break;

    $ret = -1;
    exec("grep 'OVER_QUERY_LIMIT' addr_trans/{$sha1_fn}", $null, $ret);
    if ($ret == 0 ) #found!
      die("OVER_QUERY_LIMIT\n");
    usleep(500000);
  }
}
touch('/tmp/all_done');
