var geocoder;
var map;
var infowindows = [];
var timeout_request_trigger;
var timeout_daterange_slider;
var timeout_map_slider;
var center;
var radius;
var at = 0;
var types = '1,2,3,4,5,6';
var marker_arr = [[], [], [], [], [], [], []];

var update_map = function(opts) {
    if (radius > 15000)
        return;
    
    
    var t_arr = opts['types'].split(',');
    for (t in t_arr) {
        if (! t_arr[t])
            continue;
        
        $.ajax({
            url: '//140.115.157.87:18080/map_interval', type: 'get', dataType: 'json',
            data: 'c=' + center + '&r=' + radius + '&at=' + Math.floor(opts['at']/1000) + '&t=' + t_arr[t],
            statusCode: {
                200: function(data) {
                    if(opts['clearoverlays'] == true)
                        hideMarkers(data[0]['class'], true);
                    
                    $.each(data, function(k, v) {
                        var pos = new google.maps.LatLng(v['latlng'].split(',')[0], v['latlng'].split(',')[1]);
                        for(a in marker_arr[v['class']]) {
                            if (pos.lat() == marker_arr[v['class']][a].getPosition().lat()) {
                                //console.log('dup ' + pos.lat());
                                return;
                            }
                        };

                        //console.log('add ' + pos.lat());

                        var infowindow = new google.maps.InfoWindow({
                            content: '<div class="info">名稱 ' + v['name'] + '<br>地址 ' + v['addr'] + '<br>統一編號 ' + v['sn'] + '<br>資本額 ' + v['capital'] + '</div>'
                        });

                        var marker = new google.maps.Marker({
                            map: map,
                            position: pos,
                            icon: '/img/marker' + v['class'] + '.png'
                        });

                        google.maps.event.addListener(marker, 'click', function() {
                            clearInfowindows();
                            infowindow.open(map, marker);
                            infowindows.push(infowindow);
                        });
                        marker_arr[v['class']].push(marker);
                    })
                }
            }
        });
    }
}

var update_chart = function() {
    if (radius > 15000)
        return;
    
    var slider_start = Math.round($('#daterange-slider').val()[0]/1000);
    var slider_end   = Math.round($('#daterange-slider').val()[1]/1000);
    var range = slider_start + ',' + slider_end;

    $.each(['capital', 'num'], function (series_id, json_name) {
        $.each(notation, function(b, a) {
            $.ajax({url: '//140.115.157.87:18080/chart_'+json_name, type: 'get', dataType: 'json',
                    data: a['param'] + '&range=' + range + '&c=' + center + '&r=' + radius,
                statusCode: {
                    200: function(data) {
                        var data_series = [];
                        $.each(data, function(k, v) {
                            data_series[k] = [v['timestamp'] * 1000, Math.round(v['value'])];
                        })
                        $(a['element']).highcharts().series[series_id].setData(data_series);
                    }
                }
            });
        });
    });
}

var sync_map_slider_with_daterange_slider = function(opts) {
    $("#map-slider").noUiSlider({
        range: {
            min: get_slider_token($("#daterange-slider").val(), 0),
            max: get_slider_token($("#daterange-slider").val(), 1)
        },
        step: 7 * 24 * 60 * 60 * 1000,
        start: get_slider_token($("#daterange-slider").val(), 0),
        connect: false,
        serialization: {
            lower: [
                $.Link({
                    target: $("#map-timeslot"),
                    method: setDate
                })
            ],
            format: {
                decimals: 0
            }
        }
    }, opts['updates']);

    $('#map-slider').trigger('slide');
    at = $('#map-slider').val();
}

var init_googlemaps = function() {
    geocoder = new google.maps.Geocoder();
    var pos = new google.maps.LatLng(25.0505073, 121.5282984);
    var mapOptions = {
        zoom: 17,
        center: pos,
        mapTypeControl: false
    }

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function(position) {
                pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(pos);
                map.setZoom(17);
            }
        )
    }

    google.maps.event.addListener(map, 'bounds_changed', function() {
        clearTimeout(timeout_request_trigger);
        timeout_request_trigger = setTimeout(function(){
            var r2 = map.getCenter();
    	    var r1 = new google.maps.LatLng(r2.lat(), map.getBounds().getNorthEast().lng());
	        radius = Math.round(google.maps.geometry.spherical.computeDistanceBetween(r1, r2));
            center = r2.lat() + "," + r2.lng();
        	console.log("center:" + center + "; radius:" + radius);

        	update_map({at: at, types: types});
            
            if ($('.panel').hasClass('active'))
                update_chart();
        }, 1000);
    });
}

var clearInfowindows = function() {
    for (var i = 0; i < infowindows.length; i++ ) {
        infowindows[i].setMap(null);
    }
    infowindows.length = 0;
}

var showMarkers = function(idx) {
    for (var i = 0; i < marker_arr[idx].length; i++ ) {
        marker_arr[idx][i].setMap(map);
    }
}

var hideMarkers = function(idx, remove) {
    for (var i = 0; i < marker_arr[idx].length; i++ ) {
        marker_arr[idx][i].setMap(null);
    }
    if (remove == true)
      marker_arr[idx].length = 0;
}

var clearOverlays = function() {
    for (var j = 0; j < 6 ; j++ ) {
      hideMarkers(j, true);
    }
}

var codeAddress = function() {
    var address = document.getElementById('address').value;
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            clearOverlays();
            map.setCenter(results[0].geometry.location);
            map.fitBounds(results[0].geometry.viewport);
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

var notation = [
    {element: '#c_overall', caption: '總覽', description: '各行業統計資料', param: 't=0'},
    {element: '#c_type1', caption: '食', description: '行業統計資料', param: 't=1'},
    {element: '#c_type2', caption: '衣', description: '行業統計資料', param: 't=2'},
    {element: '#c_type3', caption: '住', description: '行業統計資料', param: 't=3'},
    {element: '#c_type4', caption: '行', description: '行業統計資料', param: 't=4'},
    {element: '#c_type5', caption: '育', description: '行業統計資料', param: 't=5'},
    {element: '#c_type6', caption: '樂', description: '行業統計資料', param: 't=6'}
];

var init_component = function() {
    $.each(notation, function(b,a) {
        init_chart(a);
    });
    init_calc_chart();

    $("#daterange-slider").noUiSlider({
        range: {
            min: timestamp('2006/1/1'),
            max: timestamp('2014/7/31')
        },
        step: 7 * 24 * 60 * 60 * 1000,
        start: [ timestamp('2012/8/1'), timestamp('2014/7/31') ],
        connect: true,
        serialization: {
            lower: [
                $.Link({
                    target: $("#event-start"),
                    method: setDate
                })
            ],
            upper: [
                $.Link({
                    target: $("#event-end"),
                    method: setDate
                })
            ],
            format: {
                decimals: 0
            }
        }
    });

    sync_map_slider_with_daterange_slider({updates: false});

    $('#daterange-slider').on('set', function() {
        clearTimeout(timeout_daterange_slider);
        timeout_daterange_slider = setTimeout(function() {
            update_chart();
            sync_map_slider_with_daterange_slider({updates: true});
        }, 1000);
    });

    $('#map-slider').on('slide', function() {
        clearTimeout(timeout_map_slider);
        timeout_map_slider = setTimeout(function() {
            at = $('#map-slider').val();
            update_map({at: at, types: types, clearoverlays: true});
        }, 150);
    });

    $('#map-marker-types input').on('change', function(obj) {
        types = $('#map-marker-types').serialize().replace(/types=/g, '').replace(/\&/g, ',');

        var click_on = $(obj.currentTarget).val();
        if (types.search(click_on) != -1) {
          update_map({at: at, types: click_on, clearoverlays: false});
          showMarkers(click_on);
        }

        for (var i = 1 ; i <= 6 ; i++)
          if (types.search(i) == -1)
            hideMarkers(i, false);
    });

    $('#calc_form').on('submit', function(obj) {
        param = $('#calc_form').serialize();
        $.ajax({url: '//140.115.157.87:18080/business_analyze', type: 'get', dataType: 'json',
                data: param + '&c=' + center + '&r=' + radius,
            statusCode: {
                200: function(data) {
                    var data_series = [];
                    var data_series2 = [];
                    $.each(data, function(k, v) {
                        data_series[k] = [v['timestamp'] * 1000, parseFloat(v['value'])];
                        data_series2[k] = [v['timestamp'] * 1000, 1 - parseFloat(v['value'])];
                    })
                    console.log(data_series);
                    $('#c_calc').highcharts().series[0].setData(data_series);
                    $('#c_calc').highcharts().series[1].setData(data_series2);
                }
            }
        });
        return false;
    });
}

var get_slider_token = function(val, idx) {
    return parseInt(val.toString().split(',')[idx]);
}

var timestamp = function(str){
    return new Date(str).getTime();
}

var formatDate = function(date) {
    return date.getFullYear() + " / " + (date.getMonth()+1);
}

var setDate = function(value){
    $(this).html(formatDate(new Date(+value)));
}

var toggle_panel_stage1 = function(x) {
    $(x).toggleClass('active');

    if ($(x).hasClass('active'))
        update_chart();
}

var toggle_panel_stage2 = function(x) {
    $(x).toggleClass('active2');
    $('.lock').toggleClass('yes');

    $.each(notation, function(b, a) {
        $(a['element']).css({'opacity': 0});
    });

    setTimeout(function() {
        $(window).resize();        
        setTimeout(function() {
            $.each(notation, function(b, a) {
                $(a['element']).css({'opacity': 1});
            });
        }, 100);
    }, 1000);
}

$(function() {
    init_component();
    init_googlemaps();
    if(document.baseURI.search('debug') < 0)
        $('.debug').hide();
});
