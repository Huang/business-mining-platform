#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PyPDF2 import PdfFileReader
import pandas as pd
import pdftableextract as pdf

cols = [u"序號", u"統一編號", u"公司名稱", u"公司所在地", u"代表人", u"資本額", u"核准設立日期"]

def Q2B(uchar):
    if len(uchar) != 1:
        raise TypeError,'expected a character, but a string found!'
  
    inner_code = ord(uchar)
    if inner_code == 0x3000:
        inner_code = 0x0020
    else:  
        inner_code -= 0xfee0
    if inner_code < 0x0020 or inner_code > 0x7e:
        return uchar

    return unichr(inner_code)

def stringQ2B(ustring):
    return ''.join([Q2B(uchar) for uchar in ustring])

def main(argv):
    pdfFileName = argv[1]
    pdfFile = PdfFileReader(open(pdfFileName, "rb"))
    print "%s has %d pages." % (pdfFileName, pdfFile.getNumPages())

    pages = range(pdfFile.getNumPages()) 
    pages = [str(i+1) for i in pages]

    data = []

    for p in pages:
        cells = pdf.process_page(pdfFileName, p)

        #flatten the cells structure
        #cells = [item for sublist in cells for item in sublist ]

        #without any options, process_page picks up a blank table at the top of the page.
        #so choose table '1'
        rows = pdf.table_to_list(cells, pages)[int(p)]

        li = []
        for r in rows:
            r[0] = r[0].replace(",", "")
            if r[0].isdigit():
                r[2] = r[2].replace(" ", "")
                r[3] = stringQ2B(r[3].replace(" ", "").decode("utf-8"))
                r[4] = r[4].replace(" ", "")
                r[5] = r[5].replace(",", "")
                li.append(r)

        #pdf.output(cells, pages, table_csv_filename = sys.stdout)

        #li is a list of lists, the first line is the header, last is the footer (for this table only!)
        #column '0' contains store names
        #row '1' contains column headings
        #data is row '2' through '-1'

        pageData = pd.DataFrame(li, columns=cols)
        print pageData.to_csv(index=False, encoding="utf-8")
        data.append(pageData)

    allData = pd.concat(data)
    allData.to_csv("result.csv", index=False, encoding="utf-8")

if __name__ == "__main__":
    main(sys.argv)
